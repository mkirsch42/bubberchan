# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # Use Ubuntu 14.04 Trusty Tahr 64-bit as our operating system
  config.vm.box = "ubuntu/trusty64"

  # Configurate the virtual machine to use 2GB of RAM
  config.vm.provider :virtualbox do |vb|
    vb.customize ["modifyvm", :id, "--memory", "2048"]
  end

  # Forward the Rails server default port to the host
  config.vm.network :forwarded_port, guest: 3000, host: 3000

  # Use Chef Solo to provision our virtual machine
  config.vm.provision :chef_solo do |chef|

    chef.cookbooks_path = ["cookbooks", ".cookbooks"]

    chef.add_recipe "apt"
    chef.add_recipe "nodejs"
    chef.add_recipe "ruby_build"
    chef.add_recipe "ruby_rbenv::system"
    chef.add_recipe "sqlite"
    chef.add_recipe "postgresql::server"
    chef.add_recipe "git"
    chef.add_recipe "imagemagick"
    chef.add_recipe "imagemagick::devel"

    # Install Ruby 2.2.1 and Bundler
    # Set an empty root password for MySQL to make things simple
    chef.json = {
      postgresql: {
        config: {
          listen_addresses: "*",
          port: "5432"
        },
        pg_hba: [
          {
            type: "local",
            db: "postgres",
            user: "postgres",
            addr: nil,
            method: "trust"
          },
          {
            type: "host",
            db: "all",
            user: "all",
            addr: "0.0.0.0/0",
            method: "md5"
          },
          {
            type: "host",
            db: "all",
            user: "all",
            addr: "::1/0",
            method: "md5"
          }
        ],
        password: {
          postgres: "password"
        }
      },
      git: {
        prefix: "/usr/local"
      },
      rbenv: {
          rubies: [
            "2.2.4"
          ],
          global: "2.2.4",
          gems: {
            "2.2.4" => [
              { name: "bundler" },
              { name: "rake" },
              { name: "rails" }
            ]
          }
      }
    }
  end
end
