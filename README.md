# Bubberchan

An anonymous image board built on Rails

## Requirements

[Vagrant](https://www.vagrantup.com/downloads.html)

[Ruby](https://www.ruby-lang.org/en/downloads/)

[RubyGems](https://rubygems.org/pages/download)

[VirtualBox](https://www.virtualbox.org/wiki/Downloads)

Install the needed plugins in gems with the following commands:

    vagrant plugin install vagrant-vbguest
    vagrant plugin install vagrant-librarian-chef-nochef
    gem install librarian-chef


## Installation

Run the following commands in the root directory:

    librarian-chef install
    vagrant up


To run the server, run `vagrant ssh` or PuTTY to 127.0.0.1, port 2222, and run:

    cd /vagrant
    bundle
    rails s

Alternatively, just run `up` in the root of the application.

The server will be running on 0.0.0.0:3000

## TODO

These seriously need to be done.

- [ ] Switch from SQLite to PostreSQL
- [ ] Secure login

## License

    Copyright (C) 2015  Mathew Francis Kirschbaum

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

![GNU GPLv3 Logo](http://www.gnu.org/graphics/gplv3-127x51.png)
