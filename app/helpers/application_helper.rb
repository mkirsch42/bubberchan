# Bubberchan: an anonymous image board built on Rails
# Copyright (C) 2015  Mathew Francis Kirschbaum
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module ApplicationHelper
  extend ActionView::Helpers::SanitizeHelper
  def title(page_title)
    content_for :title, page_title.to_s
  end

  def escape(html)
    sanitize html, tags: %w(a)
  end

  def theme
    return 'light' unless cookies[:light].nil?
    'dark'
  end

  def meta
    @meta = @meta || Metadatum.first || Metadatum.create
  end
end
