# Bubberchan: an anonymous image board built on Rails
# Copyright (C) 2015  Mathew Francis Kirschbaum
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class CommentsController < ApplicationController
  before_action :validate_board

  def create
    post = Post.find_by! board_id: @board.id, id: params[:post_id]
    comment_params = params.require(:comment).permit(:body)
    @comment = Comment.create(comment_params.merge(post_id: post.id))
    redirect_to post_full_path(post)
  end

  def destroy
    Comment.destroy(params[:id]) if admin?
    redirect_to post_full_path(Post.find params[:post_id])
  end
end
