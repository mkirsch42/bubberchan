# Bubberchan: an anonymous image board built on Rails
# Copyright (C) 2015  Mathew Francis Kirschbaum
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class PostsController < ApplicationController
  before_action :validate_board

  def index
    render :index
  end

  def create
    post_params = params.require(:post).permit(:title, :body, :image)
    @new_post = Post.create(post_params.merge(board_id: @board.id))
    puts params.to_s
    redirect_to posts_path
  end

  def new
    render :new
  end

  def destroy
    Post.destroy(params[:id]) if admin?
    redirect_to posts_path
  end

  def show
    @post = Post.find_by id: params[:id], board_id: @board.id
    render :show
  end

  def edit
  end

  def update
  end
end
