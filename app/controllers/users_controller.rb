# Bubberchan: an anonymous image board built on Rails
# Copyright (C) 2015  Mathew Francis Kirschbaum
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class UsersController < ApplicationController
  def create
    redirect_to root_path unless admin?
    @user = User.create(params.require(:user).permit(:username, :password))
    redirect_to admin_user_path(@user)
  end

  def newroot
    redirect_to root_path if User.first
    password = params.require(:root).permit(:password)
    @user = User.create({ username: 'root', admin: true }.merge password)
    session[:user_id] = @user.id
    redirect_to admin_metadata_path
  end
end
