# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151220192034) do

  create_table "boards", force: :cascade do |t|
    t.string   "title",       limit: 32
    t.string   "shortname",   limit: 4
    t.text     "description", limit: 8192
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "comments", force: :cascade do |t|
    t.text     "body"
    t.integer  "post_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "comments", ["post_id"], name: "index_comments_on_post_id"

  create_table "metadata", force: :cascade do |t|
    t.string   "site_title"
    t.string   "index_page_name"
    t.text     "motd"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "moderators", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "board_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "moderators", ["board_id"], name: "index_moderators_on_board_id"
  add_index "moderators", ["user_id"], name: "index_moderators_on_user_id"

  create_table "posts", force: :cascade do |t|
    t.string   "title",      limit: 32
    t.text     "body",       limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image",      limit: 255
    t.integer  "board_id"
  end

  add_index "posts", ["board_id"], name: "index_posts_on_board_id"

  create_table "users", force: :cascade do |t|
    t.string   "username",        limit: 255
    t.string   "password_digest", limit: 255
    t.boolean  "admin",                       default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
